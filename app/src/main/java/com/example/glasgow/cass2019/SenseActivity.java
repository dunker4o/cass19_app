package com.example.glasgow.cass2019;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class SenseActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 101;

    // Attempt N2 from:
    // http://solderer.tv/data-transfer-between-android-and-arduino-via-bluetooth/

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private InputStream inStream = null;
    private byte[] buffer = new byte[256];
    private int bytes;
    // SPP UUID service
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // MAC-address of the Bt module
    private static String btAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sense);

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter == null)
            Toast.makeText(getBaseContext(), "Device doesn't support BT :(", Toast.LENGTH_SHORT).show();
        if(!btAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        if (pairedDevices.size() > 0){
            // This loop displays all paired devices for this phone.
            // Serves only debug purposes.
            // TODO: Comment out in live app version! But leave the name comparison.
            for(BluetoothDevice device : pairedDevices){
                String deviceName = device.getName();
                String deviceAddress = device.getAddress();
                Toast.makeText(SenseActivity.this, deviceName + " - " +deviceAddress, Toast.LENGTH_SHORT).show();

                if (deviceName.equals("HC-06")){
                    btAddress = deviceAddress;
                }
            }
        } else{
            Toast.makeText(SenseActivity.this, "No devices paired!", Toast.LENGTH_SHORT).show();
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException{
        if(Build.VERSION.SDK_INT >= 10){
            try{
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] {UUID.class});
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e){
                Toast.makeText(getBaseContext(), "Could not create Insecure RFComm channel! Very :(", Toast.LENGTH_SHORT).show();
            }
        }
        return device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    @Override
    public void onResume(){
        super.onResume();

        // Set up a pointer to the remote node
        BluetoothDevice device = btAdapter.getRemoteDevice(btAddress);

        // Two things are needed for connection:
        // 1) MAC address of the other device
        // 2) Service ID or UUID. We are using UUID for SPP
        try{
            btSocket = createBluetoothSocket(device);
        } catch (IOException e){
            Toast.makeText(getBaseContext(),"onResume() and socket creation failed: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        // Not that it is called somewhere but double check it is not going on, as it is
        // resource sensitive.
        btAdapter.cancelDiscovery();

        // Establish the connection. It will block until the connection is established.
        try{
            btSocket.connect();
            Toast.makeText(getBaseContext(), "Connection established!",Toast.LENGTH_SHORT).show();
        } catch (IOException e){
            try{
                btSocket.close();
            } catch (IOException e2){
                Toast.makeText(getBaseContext(), "Error on closing btSocket: "+e2.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        // Creating the streams for reading and sending data

        try{
            outStream = btSocket.getOutputStream();
            inStream = btSocket.getInputStream();
        } catch(IOException e){
            Toast.makeText(getBaseContext(), "Stream generation error: " +e.getMessage(), Toast.LENGTH_LONG).show();
        }

        updateHandler.postDelayed(updateValue, 200);
    }

    @Override
    public void onPause(){
        super.onPause();

        if(outStream != null){
            try{
                outStream.flush();
            } catch(IOException e){
                    Toast.makeText(getBaseContext(),"onPause() failed to flush outStream: "+e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }

        try{
            btSocket.close();
        } catch (IOException e2){
            Toast.makeText(getBaseContext(), "Error on closing btSocket: "+e2.getMessage(), Toast.LENGTH_LONG).show();
        }

        updateHandler.removeCallbacks(updateValue);
    }

    private void sendData(String message){
        byte[] msgBuffer = message.getBytes();

        try{
            outStream.write(msgBuffer);
        } catch(IOException e){
            Toast.makeText(getBaseContext(), "Exception during write: "+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // Continuous update service based on SO
    // https://stackoverflow.com/questions/28059985/how-to-run-a-function-all-the-time-continuously-until-the-activity-is-in-foregro
    Runnable updateValue = new Runnable(){
      @Override
        public void run(){
          TextView valueBox = (TextView) findViewById(R.id.textView3);
          DataInputStream tInput = new DataInputStream(inStream);
          try {
              bytes = tInput.read(buffer);
          } catch(IOException e){
              Toast.makeText(getBaseContext(), "Reading exception: "+e.getMessage(),Toast.LENGTH_SHORT).show();
          }

          String readMessage = new String(buffer, 0, bytes);
          valueBox.setText(readMessage);

          updateHandler.postDelayed(updateValue, 200);
      }
    };

    Handler updateHandler = new Handler();

    public void connectDevice(View view){
        TextView valueBox = (TextView) findViewById(R.id.textView3);
        // TODO: Change this field to take the value passed from the Bluetooth sensor
        /* Generate a string based on current time and display it in the results field. **/
        Date currentTime = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String newValue = dateFormat.format(currentTime);
        valueBox.setText(newValue);
        // This is where the magic read happens !!! //
        // Reading based on SO answer:
        // https://stackoverflow.com/questions/25443297/how-to-read-from-the-inputstream-of-a-bluetooth-on-android

        /** These lines have been extracted in a Runnable object which
         *  has a handler a few lines above from here
         *
        DataInputStream tInput = new DataInputStream(inStream);
        try {
            bytes = tInput.read(buffer);
        } catch(IOException e){
            Toast.makeText(getBaseContext(), "Reading exception: "+e.getMessage(),Toast.LENGTH_SHORT).show();
        }

        String readMessage = new String(buffer, 0, bytes);
        sendData(newValue);
        valueBox.setText(readMessage);
         **/
    }

}

