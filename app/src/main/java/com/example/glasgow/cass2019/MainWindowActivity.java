package com.example.glasgow.cass2019;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainWindowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_window);
    }

    /** Function to be called when the user taps this particular button **/
    public void showSenseView(View view){
        // Do something in response to the button press
        Intent intent = new Intent(this, SenseActivity.class);
        startActivity(intent);
    }

    /** Function that takes you to the settings activity **/
    public void showSettings(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /* Show Setup activity */
    public void showSetup(View view){
        Intent intent = new Intent(this, SetupActivity.class);
        startActivity(intent);
    }

    /** Jump to About window **/
    public void showAbout(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    /** Jump to Debug window **/
    public void showDebug(View view){
        Intent intent = new Intent(this, DebugActivity.class);
        startActivity(intent);
    }

    /** Jump to Print window **/
    public void showPrint(View view){
        Intent intent = new Intent(this, PrintActivity.class);
        startActivity(intent);
    }
}

